import os
import pickle
import spacy

from flask import Flask, request
from flask_cors import CORS, cross_origin

from principle_ca import search_appeal_court_data
from principle_sc import search_supreme_court_data
from helper import generate_id, get_next_sentence, get_previous_sentence


model = spacy.load('en_lawpavilion_ner')

app = Flask(__name__)
cors = CORS(app)

@app.route("/")
def index():
    return ("Lawpavilion Law, Case & Principle Extraction API.")

@app.route('/cases/', methods=['POST'])
@cross_origin()
def extract_cases():
    sessionId = generate_id()

    content = request.form['content']
    result = []
    case_details = {}

    doc = model(content)
    for ent in doc.ents:
        if ent.label_ == "CASE":
            id = generate_id()
            paragraph = str(get_previous_sentence(doc, ent.start)) + \
                str(ent.sent) + str(get_next_sentence(doc, ent.start))
            case_details[id] = [ent.text, paragraph]
            result.append({'case_title': ent.text, 'entity_type': ent.label_,
                          "caseId": id, "sessionId": sessionId})

    if bool(case_details) == True:
        pickle.dump(case_details, open(os.path.join('tmp', sessionId), "wb"))
        return {'data': result}


@app.route('/laws/', methods=['POST'])
@cross_origin()
def extract_laws():
    result = []
    content = request.form['content']
    doc = model(content)

    for ent in doc.ents:
        if ent.label_ == "LAW":
            result.append({'cited_law': ent.text, 'entity_type': ent.label_})

    return {'data': result}


@app.route('/case-principle/', methods=['POST'])
@cross_origin()
def get_case_principle():
    caseId = request.form['caseId']
    sessionId = request.form['sessionId']
    tmp_db = pickle.load(open(os.path.join('tmp',  sessionId),  "rb"))

    case_title = tmp_db[caseId][0]
    paragraph = tmp_db[caseId][1]

    appeal_court_result = search_appeal_court_data(case_title, paragraph)
    if appeal_court_result == str:
        return search_supreme_court_data(case_title, paragraph)
    else:
        return appeal_court_result


if __name__ == '__main__':
    app.run(host='0.0.0.0')
