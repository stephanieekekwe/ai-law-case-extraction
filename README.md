# LAW, CASE & PRINCIPLE EXTRACTION API.
This Application Extracts Laws and Cases cited in a document, it also predicts the Principle of a cited Case.

This API was built using SpaCy's Custom Named Entity Recognition model.

### Model Building Procedure
1. Manual Data Creation was done usign SpaCy's Phrase Matcher. The code can be found on the <b>Manual-Data-Creation/develop</b> branch.
2. Data created from the previous step was used to create a Sense2vec Model. The code can be found on the <b>sense2vec-model/develop</b> branch.
3. The Sense2vec model was used in the Prodi.gy Annotation tool, to enable automatic annotation.
4. Data Extracted from the previous step was inputed SpaCy's NER Pretrained Model to build the final model. The code can be found <b>model-package/develop</b> branch.

## GETTING STARTED

### Pre-requisites and Local development

Developers working on this project should have Python3 and pip installed on their local machines

Create a Virtual Enviroment using pip and run the virtual enviroment
On Linux:
```
source env/bin/activate
```

On Windows:
```
env/scripts/activate
```

From the base folder run

<b>The requirement includes <b>en_lawpavilion_ner-(version)</b> which can be downloaded from the lawpavilion's AI team's server.</b>
```
pip install -r requirements.txt
```

On Linux:
```
flask run
```

On Windows:
```
flask run
```

The application is run on http://0.0.0.0:5000/ by default

## API ENPOINTS

### [POST] /laws
> The /law endpoint takes one input (case content).
- General:
    - Request should be made with a case as content which should be sent as form data.
    - Returns a list of cited laws and entity type in JSON Format.

- Sample Request:

```
curl \
-X POST \
-F "content=The facts of the case were that the Accused was alleged to have stabbed the deceased to death and thereby murdered him. At the conclusion of trial, the learned trial judge found the Accused guilty for the offence of murder and was accordingly convicted." \
http://127.0.0.1:5000/laws
```

- Sample Result

```
{
    "data": [
        {
            "cited-law": "Section 34 of the Constitution of the Federal Republic of Nigeria, 1999",
            "entity-type": "LAW"
        },
        {
            "cited-law": "Sections 34, 35, 36, 41 and 46 (1) (2) & (3) of the 1999 Constitution of the Federal Republic of Nigeria",
            "entity-type": "LAW"
        }
    ]    
}
```
### [POST] /cases
> The /cases endpoint takes one input (case content).
- General:
    - Request should be made with a case as content which should be sent as form data <b>WITH TEXT FORMATING MANTAINED</b>.
    - Returns a list containing case-title, caseId, entity-type and sessionId in JSON Format.

- Sample Request:

```
curl \
-X POST \
-F "content=The facts of the case were that the Accused was alleged to have stabbed the deceased to death and thereby murdered him. At the conclusion of trial, the learned trial judge found the Accused guilty for the offence of murder and was accordingly convicted." \
http://127.0.0.1:5000/cases
```

- Sample Result

```
}
    "data": [
        {
            "case-title": "Abiodun vs. CJ Kwara State (2007)",
            "caseId": "OBA6CM2I",
            "entity-type": "CASE",
            "sessionId": "FR3G4JEY"
        },
        {
            "case-title": "Fajemirokun vs. Commercial Bank Nigeria Limited & Anor (2009)",
            "caseId": "L7SG87Y4",
            "entity-type": "CASE",
            "sessionId": "FR3G4JEY"
        }
    ]
}   
```

### [POST] /cases-principle
> The /cases-principle endpoint takes two inputs caseId and sessionID gotten from a single case result (from the /case endpoint).
- General:
    - Request should be made with caseId and sessionID, which should be sent as form data.
    - Returns a dictionary percentage, principle-key and principle, in JSON Format.


- Sample Request:

```
curl \
-X POST \
-F "caseId="X9I28Z4C"&sessionId="4HYQVQZT"\
http://127.0.0.1:5000/case-principle
```
- Sample Result
```
{
    "percentage": 97.54697422425643,
    "principle": The second facet of this issue is whether or not the learned trial Judge was right to have failed to consider and render an opinion on the question of whether the Hon. Chief Judge was right to have gone ahead to inaugurate a panel to investigate the appellant when to his knowledge a suit seeking mandatory and injunctive orders to prevent him from doing so was pending to his knowledge. I quite agree with the learned Senior Advocate that the learned trial Judge carefully avoided making a pronouncement one way or another on this aspect of the issue. I must take the position that a Judge is obliged to consider and determine all issues properly raised before him which are not hypothetical, See Okonkwo Okonji &amp; Ors. v. George Njokanma &amp; Ors. (1999) 12 SCNJ 259, (1999) 14 NWLR (Pt. 638) 250; Attorney General, Federation v. A.I.C. Ltd. (2000) 6 SCNJ 171; (2000) 10 NWLR (Pt. 675) 293. However, I do not buy the argument of learned Senior Advocate that the failure of the learned trial Judge to pronounce on this aspect of the issue has per se occasioned miscarriage of justice. The effect when an issue is raised by a party and the Court omits to pronounce on it for whatever reason depends on the circumstances of each case. It is my view that a look at the process filed at the lower Court shows that the Court was not called upon to determine that aspect of the issue that is whether the Chief Judge ought not to have proceeded to institute the panel after he was served with Court process. It became an ancillary issue during the trial since the Chief Judge proceeded to inaugurate the investigative panel after the case had been filed and he was served with process. In such a situation, being an ancillary issue which came up during the course of the trial itself, I do not think the failure of the learned trial Judge to comment on it has occasioned miscarriage of justice in the circumstances of this case. See Ebe Ebe Uka &amp; Anor. v. Chief Kalu Okorie Irolo &amp; Ors. (2002) 7 SCNJ 137: (2002) 14 NWLR (Pt. 786) 195. It is only where the issue is a vital and momentous one to the determination of the questions placed before the Court that the failure to resolve it vitiates a judgment. See Benneth Ude Agu v. Maxwell Nnadi (2002) 12 SCNJ 238; (2002) 18 NWLR (Pt. 798) 103.,
    "principle_key": 202592
}
```
