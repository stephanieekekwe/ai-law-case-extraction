import string
import random

def generate_id():
    args = string.ascii_uppercase + string.digits
    unique_id = ''.join(random.choice(args) for i in range(8))
    return unique_id

def get_previous_sentence(doc, token_index):
    if doc[token_index].is_space == False:
        return str(get_previous_sentence(doc, doc[doc[token_index].sent.start - 1].sent.start)) + str(doc[doc[token_index].sent.start - 1].sent)

def get_next_sentence(doc, token_index): 
    if doc[token_index].is_space == False:
            return str(doc[doc[token_index].sent.end + 1].sent) + str(get_next_sentence(doc, doc[doc[token_index].sent.end + 1].sent.start))
    else:
        if doc[token_index].sent.end + 1 >= len(doc):
            return ''
        else:
            return doc[doc[token_index].sent.end + 1].sent
