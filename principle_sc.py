from fuzzywuzzy import process, fuzz
import pandas as pd
import numpy as np
import spacy
nlp = spacy.load('en_lawpavilion_ner')

supereme_court_data = pd.read_csv(r'judge_sc.csv')

def search_supreme_court_data(case_extract, summary):
    case_title = case_extract.split(' (')[0]
    _summary = nlp(summary)

    short_case_title_results = process.extract(
        case_title, supereme_court_data['case_title'], scorer=fuzz.token_sort_ratio)
    long_case_title_results = process.extract(
        case_title, supereme_court_data['case_title2'], scorer=fuzz.token_sort_ratio)
    all_cases = short_case_title_results + long_case_title_results

    top_cases = []

    for items in all_cases:
        percentage = items[1]
        if percentage > 85:
            principle_key = np.asscalar(supereme_court_data['pk'][items[2]])
            principle = supereme_court_data[supereme_court_data['pk']
                                          == principle_key]['principle'].values[0]
            top_cases.append(
                {'principle_key': principle_key, 'principle': principle})

    if len(top_cases) == 0:
        return('No principles Found')
    else:
        higest_similarity_percent = 0
        best_result = {}
        for content in top_cases:
            principle = nlp(content['principle'])
            percentage = ((_summary.similarity(principle)) * 100)
            if percentage > higest_similarity_percent:
                higest_similarity_percent = percentage
                best_result = content
                best_result.update({'percentage': percentage})

    return best_result
